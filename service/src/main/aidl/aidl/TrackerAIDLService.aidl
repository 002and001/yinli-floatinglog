// TrackerAIDLService.aidl
package aidl;

// Declare any non-default types here with import statements

interface TrackerAIDLService {

        void showLogView();
        void updateLogs(String log);
        void hideLogs();

}
