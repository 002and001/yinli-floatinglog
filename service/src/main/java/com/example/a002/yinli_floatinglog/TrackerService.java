package com.example.a002.yinli_floatinglog;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;


public class TrackerService extends Service {

    private static final String TAG = "TrackerService";
    private TrackerServiceImplement stub;

    public TrackerService() {
        super();
        Log.d(TAG, "TrackerService: extuted");
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind: excuted");
        return stub;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: excuted");
        stub = new TrackerServiceImplement(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: excuted");
        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        Log.d(TAG, "onDestroy: excuted");
        super.onDestroy();
    }

}
