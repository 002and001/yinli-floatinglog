package com.example.a002.yinli_floatinglog;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.linsh.utilseverywhere.ScreenUtils;

/**
 * Created by jinliangshan on 16/12/26.
 */
public class LogFloatingView extends LinearLayout {

    public static final String TAG = "FloatingView";


    private final Context mContext;
    private final WindowManager mWindowManager;
    private LinearLayout mLinearLayout;
    private TextView mTvLogName;
    private ImageView mIvZoomIn;
    private ImageView mIvZoomOut;
    private ImageView mIvClose;

    public LogFloatingView(Context context) {
        super(context);
        mContext = context;
        mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        initView();
    }

    public void updateLogs(String log) {
        mTvLogName.setText(log);
        mTvLogName.getMeasuredHeight();
    }

    private void initView() {
        inflate(mContext, R.layout.layout_floating, this);

        mLinearLayout = (LinearLayout)findViewById(R.id.outLineartLayout);
        mIvClose = (ImageView) findViewById(R.id.iv_close);
        mIvZoomIn = (ImageView) findViewById(R.id.iv_zoomIn);
        mIvZoomOut = (ImageView) findViewById(R.id.iv_zoomOut);
        mTvLogName = (TextView) findViewById(R.id.tv_log_name);

        mIvClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: mIvClose excuted");
                mWindowManager.removeView(LogFloatingView.this);
            }
        });

        mIvZoomIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: mIvZoomIn excuted");
                LayoutParams layoutParams = (LayoutParams) mLinearLayout.getLayoutParams();

                layoutParams.width = 150;
                layoutParams.height = 200;
                mLinearLayout.setLayoutParams(layoutParams);
            }
        });


        mIvZoomOut.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: mIvZoomOut excuted");
                LayoutParams layoutParams = (LayoutParams) mLinearLayout.getLayoutParams();
                int width = ScreenUtils.getScreenWidth() - 100;
                int height = ScreenUtils.getScreenHeight() - 100;
                layoutParams.width = width;
                layoutParams.height = height;
                mLinearLayout.setLayoutParams(layoutParams);
            }
        });
    }

    Point preP, curP;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                preP = new Point((int) event.getRawX(), (int) event.getRawY());
                break;

            case MotionEvent.ACTION_MOVE:
                curP = new Point((int) event.getRawX(), (int) event.getRawY());
                int dx = curP.x - preP.x,
                        dy = curP.y - preP.y;

                WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) this.getLayoutParams();
                layoutParams.x += dx;
                layoutParams.y += dy;
                mWindowManager.updateViewLayout(this, layoutParams);

                preP = curP;
                break;
        }

        return false;
    }
}
