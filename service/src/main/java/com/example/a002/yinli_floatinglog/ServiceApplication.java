package com.example.a002.yinli_floatinglog;

import android.app.Application;

import com.linsh.utilseverywhere.Utils;


public class ServiceApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Utils.init(this);
    }
}
