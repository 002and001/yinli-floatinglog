package com.example.a002.yinli_floatinglog;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;

import com.linsh.utilseverywhere.HandlerUtils;
import com.linsh.utilseverywhere.ScreenUtils;

/**
 * Created by jinliangshan on 16/12/26.
 */
public class TrackerWindowManager {
    private final Context mContext;
    private final WindowManager mWindowManager;
    private String mLog;

    public TrackerWindowManager(Context context) {
        mContext = context;
        mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    }

    private LogFloatingView mFloatingView;
    private static final WindowManager.LayoutParams LAYOUT_PARAMS_ZOOM_OUT;
    private static final WindowManager.LayoutParams LAYOUT_PARAMS_ZOOM_IN;

    static {
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.x = 50;
        params.y = 50;
        int width = ScreenUtils.getScreenWidth() - 100;
        int height = ScreenUtils.getScreenHeight() - 100;
        params.width = width;
        params.height = height;

        params.gravity = Gravity.LEFT | Gravity.TOP;
        if (Build.VERSION.SDK_INT >= 26) {
            params.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            params.type = WindowManager.LayoutParams.TYPE_PHONE;
        }

        params.format = PixelFormat.RGBA_8888;
        params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;

        LAYOUT_PARAMS_ZOOM_OUT = params;
    }

    static {
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();

        int width = 150;
        int height = 150;

        params.x = ScreenUtils.getScreenWidth() / 2 - width / 2;
        params.y = ScreenUtils.getScreenHeight() / 2 - height / 2;

        params.width = width;
        params.height = height;

        params.gravity = Gravity.LEFT | Gravity.TOP;
        if (Build.VERSION.SDK_INT >= 26) {
            params.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            params.type = WindowManager.LayoutParams.TYPE_PHONE;
        }

        params.format = PixelFormat.RGBA_8888;
        params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;

        LAYOUT_PARAMS_ZOOM_IN = params;
    }


    public void showLogs(String log) {
        mLog = log;
        if (mFloatingView == null) {
            addView();
        }

        HandlerUtils.postRunnable(new Runnable() {
            @Override
            public void run() {
                mFloatingView.updateLogs(mLog);
            }
        });
    }

    public void addView() {
        HandlerUtils.postRunnable(new Runnable() {
            @Override
            public void run() {
                Log.i("lsh", "addView: " + Thread.currentThread().getName());
                if (mFloatingView == null) {
                    mFloatingView = new LogFloatingView(mContext);
                    mFloatingView.setLayoutParams(LAYOUT_PARAMS_ZOOM_OUT);
                }
                if (mFloatingView.getParent() == null) {
                    mWindowManager.addView(mFloatingView, LAYOUT_PARAMS_ZOOM_OUT);
                }
            }
        });
    }

    public void removeView() {
        HandlerUtils.postRunnable(new Runnable() {
            @Override
            public void run() {
                if (mFloatingView != null && mFloatingView.getParent() != null) {
                    mWindowManager.removeView(mFloatingView);
                    mFloatingView = null;
                }
            }
        });
    }

}
