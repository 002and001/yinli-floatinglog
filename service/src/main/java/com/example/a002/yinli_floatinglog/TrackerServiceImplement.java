package com.example.a002.yinli_floatinglog;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Log;

import com.linsh.utilseverywhere.ContextUtils;
import com.linsh.utilseverywhere.HandlerUtils;

import aidl.TrackerAIDLService;

public class TrackerServiceImplement extends TrackerAIDLService.Stub {


    private static final String TAG = "TrackerServiceImplement";
    private TrackerService trackerService;
    private StringBuilder logs = new StringBuilder();
    private final int maxLogsLength = 5 * 1024 * 1024;

    TrackerWindowManager mTrackerWindowManager;

    public TrackerServiceImplement(TrackerService service) {
        super();
        trackerService = service;
        initTrackerWindowManager();
        logs.setLength(maxLogsLength);
    }
    private void initTrackerWindowManager() {
        Log.d(TAG, "initTrackerWindowManager: excuted");

        if (mTrackerWindowManager == null)
            mTrackerWindowManager = new TrackerWindowManager(trackerService);
    }

    private boolean haveOverlayPermission(String packageName) {
        Log.d(TAG, "checkOverlayPermission: packageName" + packageName);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(trackerService)) {
                return false;
            }
        }
        return true;
    }

    private void openOverlayPermissionView(String packageName) {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
        intent.setData(Uri.parse("package:" + packageName));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ContextUtils.get().startActivity(intent);
    }

    @Override
    public void showLogView() throws RemoteException {
        Log.d(TAG, "showLogView: excuted");
        final String packageName = trackerService.getPackageName();
        if (haveOverlayPermission(packageName)) {
            mTrackerWindowManager.addView();
        } else {
            HandlerUtils.postRunnable(new Runnable() {
                @Override
                public void run() {
                    openOverlayPermissionView(packageName);
                }
            });
        };
    }

    @Override
    public void updateLogs(String log) throws RemoteException {
        Log.d(TAG, "updateLogs: excuted");
        if (log.length() + logs.length() > maxLogsLength) {
            logs.delete(maxLogsLength / 3, logs.length());
        } else {
            mTrackerWindowManager.showLogs(logs.insert(0, (log + "\n")).toString());
        }
    }

    @Override
    public void hideLogs() throws RemoteException {
        Log.d(TAG, "hideLogs: excuted");
        mTrackerWindowManager.removeView();
    }
}
